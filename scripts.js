

function validateForm() {

    if (document.myForm.Name.value == "") {
      alert("please provide your name");
      document.myForm.Name.focus() ;
      return false;
    }

      if (document.myForm.Surname.value == "") {
        alert("please provide your surname");
        document.myForm.Surname.focus();
        return false;
      }

      if (document.myForm.Email.value == "") {
        alert("please provide your Email");
        document.myForm.Email.focus();
        return false;
      }

     if (document.myForm.Gender.value == "") {
        alert("please provide your Gender");
        document.myForm.Gender.focus();
        return false;
      }

      if (document.myForm.Age.value == "") {
        alert("please provide your Age");
        document.myForm.Age.focus();
        return false;
      }

      if (document.myForm.Phone.value == "" ) {
        alert("please provide your Phone");
        document.myForm.Phone.focus();
        return false;
      }
    }

 let data = [];

let save = document.getElementById('save');

    save.addEventListener('click', (event) => {
      validateForm();
      data.push({
        name: document.myForm.Name.value,
        surname: document.myForm.Surname.value,
        email: document.myForm.Email.value,
        gender:document.myForm.Gender.value,
        age:document.myForm.Age.value,
        phone:document.myForm.Phone.value,
      });

      const tb = document.getElementById("tbody");
      let tr = [];
      data.forEach(item => {
        tr.push('<tr><td>' + item.name + '</td>')
        tr.push('<td>' + item.surname + '</td>')
        tr.push('<td>' + item.email + '</td>')
        tr.push('<td>' + item.gender + '</td>')
        tr.push('<td>' + item.age + '</td>')
        tr.push('<td>' + item.phone + '</td></tr>')
      })
      tb.innerHTML = tr.join("");
       event.preventDefault()
    })
